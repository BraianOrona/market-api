-- MariaDB dump 10.19  Distrib 10.4.19-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: market-api
-- ------------------------------------------------------
-- Server version	10.4.19-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `brands` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands` DISABLE KEYS */;
INSERT INTO `brands` VALUES (1,'2021-05-20 23:11:50',NULL,NULL,'asdasd'),(2,'2021-05-20 23:13:45',NULL,NULL,'pepsi'),(3,'2021-05-21 00:47:02',NULL,'2021-05-20 21:47:46','coca-cola'),(4,'2021-05-21 00:50:06','2021-05-21 19:13:27',NULL,'felfort'),(5,'2021-05-21 21:47:26',NULL,NULL,'Arcor'),(6,'2021-05-23 07:14:45',NULL,NULL,'Quilmes');
/*!40000 ALTER TABLE `brands` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `deleted` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'2021-05-19 23:40:01','2021-05-23 02:46:35','bebidas',NULL),(2,'2021-05-19 23:40:54',NULL,'holaasd',NULL),(3,'2021-05-20 00:57:35',NULL,'hola','2021-05-20 16:37:26'),(4,'2021-05-20 00:57:38','2021-05-19 22:17:09','holaasd','2021-05-20 16:22:18'),(5,'2021-05-20 20:39:25',NULL,'hola2',NULL),(6,'2021-05-20 20:44:27',NULL,'asdasd',NULL),(7,'2021-05-21 21:47:50',NULL,'Galletas',NULL),(8,'2021-05-22 11:57:06','2021-05-22 08:57:30','perfumeria','2021-05-22 08:58:14'),(9,'2021-05-23 05:46:48',NULL,'locuras',NULL);
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand_id` bigint(20) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `expiration_date` datetime DEFAULT NULL,
  `measure_unit` varchar(255) DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `observations` varchar(255) DEFAULT NULL,
  `unit_amount` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKa3a4mpsfdf4d2y6r8ra3sc8mv` (`brand_id`),
  KEY `FKog2rp4qthbtt2lfyhfo32lsw9` (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,1,1,'2021-05-13 01:44:10','2021-05-13 03:43:22','2022-05-18 22:43:22','SIXPACK','2021-05-13 03:43:22','cerveza loko','loco',50.00),(2,2,2,'2021-05-13 01:51:01','2021-05-13 03:50:22','2021-05-13 03:50:22','UNIDAD','2021-05-13 03:50:22','chocolate','chocolate blanco',100.00),(5,3,1,'2021-05-14 19:44:37',NULL,NULL,'SIXPACK',NULL,'asd','hola233',20.00),(6,2,2,'2021-05-14 23:40:14',NULL,NULL,'SIXPACK',NULL,'asdwf','ho33',20.00),(7,2,2,'2021-05-14 23:57:42',NULL,NULL,'UNIDAD','2021-05-23 02:44:57','anoc','f',120.00),(12,NULL,1,'2021-05-20 20:14:28',NULL,'2022-05-13 03:50:22','SIXPACK',NULL,'algo','anoc',20.00),(13,5,7,'2021-05-21 21:48:51',NULL,'2022-05-13 03:50:22','UNIDAD',NULL,'Galletas Xplotion','Surtidas',100.00),(14,5,7,'2021-05-22 11:52:19','2021-05-22 08:52:32','2022-05-13 03:50:22','UNIDAD','2021-05-22 08:53:02','galletas anoc','surtidas',120.00),(15,1,1,'2021-05-23 05:45:46',NULL,'2022-05-13 03:50:22','UNIDAD',NULL,'Galletas asd','Surtidas',100.00);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sale_items`
--

DROP TABLE IF EXISTS `sale_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sale_items` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `sale_id` bigint(20) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `sale_item_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK8g0sjiqs7tg055o06p6wawu39` (`product_id`),
  KEY `FK8rn4walj59c88llvcsa42y0w0` (`sale_item_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sale_items`
--

LOCK TABLES `sale_items` WRITE;
/*!40000 ALTER TABLE `sale_items` DISABLE KEYS */;
INSERT INTO `sale_items` VALUES (1,20.00,'2021-05-22 12:09:48','2021-05-22 14:08:59','2021-05-22 14:08:59',1,1,0),(2,50.00,'2021-05-23 18:34:07',NULL,NULL,2,1,NULL),(3,50.00,'2021-05-23 18:52:33',NULL,NULL,1,1,NULL),(4,50.00,'2021-05-23 19:57:28',NULL,NULL,1,1,NULL);
/*!40000 ALTER TABLE `sale_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sales`
--

DROP TABLE IF EXISTS `sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `place_id` bigint(20) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `subsidiary_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sales`
--

LOCK TABLES `sales` WRITE;
/*!40000 ALTER TABLE `sales` DISABLE KEYS */;
INSERT INTO `sales` VALUES (1,'2021-05-20 00:53:04',NULL,'2021-05-20 15:29:21',2,'vendido',3,1);
/*!40000 ALTER TABLE `sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocks`
--

DROP TABLE IF EXISTS `stocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocks` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `observations` varchar(255) DEFAULT NULL,
  `product_id` bigint(20) DEFAULT NULL,
  `subsidiary_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKff7be959jyco0iukc1dcjj9qm` (`product_id`),
  KEY `FK2o7fl1uw28dpikq3ufaq66io8` (`subsidiary_id`),
  KEY `FKg0lmdnqpu9ib3sardb33sv4nq` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocks`
--

LOCK TABLES `stocks` WRITE;
/*!40000 ALTER TABLE `stocks` DISABLE KEYS */;
INSERT INTO `stocks` VALUES (1,150.00,'2021-05-20 00:57:12',NULL,'2021-05-20 15:29:01','lala',1,3,2),(2,100.00,'2021-05-23 05:49:11','2021-05-23 16:54:46',NULL,'nose',NULL,NULL,NULL);
/*!40000 ALTER TABLE `stocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subsidiaries`
--

DROP TABLE IF EXISTS `subsidiaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `subsidiaries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `address` varchar(255) DEFAULT NULL,
  `city` bigint(20) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subsidiaries`
--

LOCK TABLES `subsidiaries` WRITE;
/*!40000 ALTER TABLE `subsidiaries` DISABLE KEYS */;
INSERT INTO `subsidiaries` VALUES (1,'tucuman',1,'2021-05-18 05:27:21','2021-05-18 07:26:44','2021-05-18 07:26:44','braian'),(3,'cereceto 5 este',2,'2021-05-18 05:30:38',NULL,'2021-05-20 15:29:44','juan'),(4,'tucuman',1,'2021-05-19 19:34:53',NULL,NULL,'braian'),(5,'tucuman',1,'2021-05-20 00:56:22',NULL,NULL,'braian'),(6,'entre rios',1,'2021-05-20 18:30:19',NULL,NULL,'asd'),(7,'entre rios',2,'2021-05-23 05:50:41',NULL,NULL,'nose');
/*!40000 ALTER TABLE `subsidiaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `subsidiary_id` bigint(20) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKh1vbau7udu15umoyf7pj0hio4` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'2021-05-15 22:25:37','2021-05-16 00:25:06','2021-05-16 00:25:06','orona','321',2,'kkk',NULL),(2,'2021-05-15 22:40:05','2021-05-16 00:25:06','2021-05-16 00:25:06','joni','321',2,'loco',NULL),(7,'2021-05-19 19:22:26',NULL,NULL,'joni','321',2,'loco',NULL),(6,'2021-05-18 05:04:49',NULL,NULL,'joni','321',2,'loco',NULL),(8,'2021-05-19 19:34:21','2021-05-22 09:00:50',NULL,'joni','321',2,'loco',NULL),(9,'2021-05-20 00:57:24',NULL,'2021-05-20 15:16:48','joni orona','5555',2,'kkk',NULL),(10,'2021-05-22 11:59:41',NULL,'2021-05-22 09:00:23','Braian orona','123456',3,'vordr',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-23 22:19:32
