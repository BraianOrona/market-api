package com.folcademy.marketapi.services;

import com.folcademy.marketapi.models.dto.ReportSaleDTO;
import com.folcademy.marketapi.models.entities.SaleEntity;
import com.folcademy.marketapi.models.entities.StockEntity;
import com.folcademy.marketapi.models.repositories.SaleRepository;
import com.folcademy.marketapi.models.repositories.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ReportsService {

    @Autowired
    private SaleRepository saleRepository;
    @Autowired
    private StockRepository stockRepository;

    public ResponseEntity<?> getSales(String dateFrom, String dateTo) throws ParseException {
        List<SaleEntity> sales = new ArrayList<>();
        sales = saleRepository.findByCreatedBetween(parseFecha(dateFrom), parseFecha(dateTo));
        //sales = saleRepository.findByCreatedBetween(parseFecha(reportSaleDTO.getDateFrom()), parseFecha(reportSaleDTO.getDateTo()));
        return new ResponseEntity<>(sales, HttpStatus.OK);
    }

    public ResponseEntity<?> getStocks(String dateFrom, String dateTo) throws ParseException {
        List<StockEntity> stocks = new ArrayList<>();
        List<StockEntity> realStocks = new ArrayList<>();
        stocks = stockRepository.findByCreatedBetween(parseFecha(dateFrom), parseFecha(dateTo));
        for (StockEntity stock : stocks){
            if (stock.getDeleted() == null){
                realStocks.add(stock);
            }
        }
        return new ResponseEntity<>(realStocks, HttpStatus.OK);
    }

    private Date parseFecha(String fecha) throws ParseException {
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-mm-dd");
        Date fechaDate = null;
        fechaDate = formato.parse(fecha);
        return fechaDate;
    }
}
