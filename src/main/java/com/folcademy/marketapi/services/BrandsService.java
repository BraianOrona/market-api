package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.BrandByIdDTO;
import com.folcademy.marketapi.models.dto.EditBrandDTO;
import com.folcademy.marketapi.models.dto.NewBrandDTO;
import com.folcademy.marketapi.models.entities.BrandEntity;
import com.folcademy.marketapi.models.mappers.BrandMapper;
import com.folcademy.marketapi.models.repositories.BrandRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Date;
import java.util.Optional;

@Service
public class BrandsService {

    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private BrandMapper brandMapper;

    //view
    public ResponseEntity<?> getBrandById(Long id){
        Optional<BrandEntity> brandEntityOptional = brandRepository.findById(id);
        if (brandEntityOptional.isEmpty()){
            throw new NotFoundException("No existe la marca");
        }
        BrandEntity brandEntity = brandEntityOptional.get();
        BrandByIdDTO brandByIdDTO = brandMapper.mapBrandEntityToBrandDTO(brandEntity);
        return new ResponseEntity<>(brandByIdDTO, HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newBrand(NewBrandDTO newBrandDTO){
        if (newBrandDTO.getName().equals("")){
            throw new BadRequestException("Error al crear la marca, debe ingresar el nombre de la marca");
        }

        try {
            BrandEntity brandEntity = brandMapper.mapBrandDTOToBrandEntity(newBrandDTO);
            brandRepository.save(brandEntity);
            return new ResponseEntity<>(brandEntity,HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editBrand (Long id, EditBrandDTO brand){
        try {
            if (!brandRepository.existsById(id))
                throw new NotFoundException("No existe la marca");
            BrandEntity brandEntity = brandRepository.findById(id).get();
            if (!brand.getName().isEmpty())
                brandEntity.setName(brand.getName());
            brandEntity.setModified(new Date());
            brandRepository.save(brandEntity);
            return new ResponseEntity<>("Marca actualizada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteBrand (Long id){
        Optional<BrandEntity> brandEntityOptional = brandRepository.findById(id);
        if (brandEntityOptional.isEmpty()){
            throw new NotFoundException("No existe la marca que quiere eliminar");
        }
        BrandEntity brandEntity = brandEntityOptional.get();
        //BrandEntity brandEntity = brandRepository.findById(id).get();
        brandEntity.setDeleted(new Date());
        brandRepository.save(brandEntity);
        return new ResponseEntity<>("Marca eliminada", HttpStatus.OK);
    }

}
