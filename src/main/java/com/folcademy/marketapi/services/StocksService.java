package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.EditStockDTO;
import com.folcademy.marketapi.models.dto.NewStockDTO;
import com.folcademy.marketapi.models.dto.StockByIdDTO;
import com.folcademy.marketapi.models.entities.StockEntity;
import com.folcademy.marketapi.models.mappers.StockMapper;
import com.folcademy.marketapi.models.repositories.StockRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class StocksService {

    private final StockRepository stockRepository;
    private final StockMapper stockMapper;

    public StocksService(StockRepository stockRepository, StockMapper stockMapper) {
        this.stockRepository = stockRepository;
        this.stockMapper = stockMapper;
    }

    //view
    public ResponseEntity<?> getStockById(Long id){
        Optional<StockEntity> stockEntityOptional = stockRepository.findById(id);
        if (stockEntityOptional.isEmpty()){
            throw new NotFoundException("No existe el stock");
        }
        StockEntity stockEntity = stockEntityOptional.get();
        StockByIdDTO stockByIdDTO = stockMapper.mapStockEntityToStockDTO(stockEntity);
        return new ResponseEntity<>(stockByIdDTO, HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newStock(NewStockDTO newStockDTO){
        if (newStockDTO.getAmount().toString().isEmpty())
            throw new BadRequestException("Error al crear stock, la cantidad no puede estar vacia");

        if (newStockDTO.getObservations().equals(""))
            throw new BadRequestException("Error al crear stock, observaciones no puede estar vacia");
        try {
            StockEntity stockEntity = stockMapper.mapStockDTOToStockEntity(newStockDTO);
            stockRepository.save(stockEntity);
            return new ResponseEntity<>(stockEntity, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editStock(Long id, EditStockDTO stock){
        try {
            if (!stockRepository.existsById(id))
                throw new NotFoundException("No existe el stock");
            StockEntity stockEntity = stockRepository.findById(id).get();
            if (!stock.getAmount().toString().isEmpty())
                stockEntity.setAmount(stock.getAmount());
            if (!stock.getObservations().isEmpty())
                stockEntity.setObservations(stock.getObservations());
            stockEntity.setModified(new Date());
            stockRepository.save(stockEntity);
            return new ResponseEntity<>("Stock actualizado", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteStock (Long id){
        Optional<StockEntity> stockEntityOptional = stockRepository.findById(id);
        if (stockEntityOptional.isEmpty())
            throw new NotFoundException("No existe el stock que quiere eliminar");
        StockEntity stockEntity = stockEntityOptional.get();
        stockEntity.setDeleted(new Date());
        stockRepository.save(stockEntity);
        return new ResponseEntity<>("Stock eliminado", HttpStatus.OK);
    }
}
