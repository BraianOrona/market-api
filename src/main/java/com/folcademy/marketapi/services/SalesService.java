package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.EditSaleDTO;
import com.folcademy.marketapi.models.dto.NewSaleDTO;
import com.folcademy.marketapi.models.dto.SaleByIdDTO;
import com.folcademy.marketapi.models.entities.SaleEntity;
import com.folcademy.marketapi.models.entities.SaleItemEntity;
import com.folcademy.marketapi.models.mappers.SaleMapper;
import com.folcademy.marketapi.models.repositories.SaleItemRepository;
import com.folcademy.marketapi.models.repositories.SaleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class SalesService {

        private final SaleRepository saleRepository;
        private final SaleMapper saleMapper;
        private final SaleItemRepository saleItemRepository;

    public SalesService(SaleRepository saleRepository, SaleMapper saleMapper, SaleItemRepository saleItemRepository) {
        this.saleRepository = saleRepository;
        this.saleMapper = saleMapper;
        this.saleItemRepository = saleItemRepository;
    }

    //view
    public ResponseEntity<?> getSaleById(Long id){
        if (!saleRepository.existsById(id))
            throw new NotFoundException("No se encontro la venta");
        Optional<SaleEntity> saleEntityOptional = saleRepository.findById(id);
        SaleEntity saleEntity = saleEntityOptional.get();
        SaleByIdDTO saleByIdDTO = saleMapper.mapSaleEntityToSaleDTO(saleEntity);
        return new ResponseEntity<>(saleByIdDTO,HttpStatus.OK);
    }

    //add
    public ResponseEntity<String> newSale(NewSaleDTO newSaleDTO){
        if (newSaleDTO.getState().isEmpty())
            throw new BadRequestException("Error al crear la venta, debe ingresar estado");
        try {
            SaleEntity saleEntity = saleMapper.mapSaleDTOToSaleEntity(newSaleDTO);
            //saleEntity.setSaleItems();
            Optional<SaleItemEntity> saleItemEntityOptional = saleItemRepository.findById(newSaleDTO.getSaleItemId());

            if (saleItemEntityOptional.isEmpty())
                throw new BadRequestException("El item vendido no existe");
            saleEntity.setSaleItems((List<SaleItemEntity>) saleItemEntityOptional.get());
            saleRepository.save(saleEntity);
            return new ResponseEntity<>("Venta creada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editSale(Long id, EditSaleDTO sale){
        try{
            if (!saleRepository.existsById(id))
                throw new NotFoundException("No existe la venta");
            SaleEntity saleEntity = saleRepository.findById(id).get();
            if (!sale.getState().isEmpty())
                saleEntity.setState(sale.getState());

            if (!sale.getPlaceId().toString().isEmpty())
                saleEntity.setPlaceId(sale.getPlaceId());

            saleEntity.setModified(new Date());
            saleRepository.save(saleEntity);
            return new ResponseEntity<>("Venta actualizada", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteSale(Long id){
        Optional<SaleEntity> saleEntityOptional = saleRepository.findById(id);
        if (saleEntityOptional.isEmpty()){
            throw new NotFoundException("No existe la venta que quiere eliminar");
        }
        SaleEntity saleEntity = saleEntityOptional.get();
        saleEntity.setDeleted(new Date());
        saleRepository.save(saleEntity);
        return new ResponseEntity<>("Venta eliminada", HttpStatus.OK);
    }
}
