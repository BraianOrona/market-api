package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.EditUserDTO;
import com.folcademy.marketapi.models.dto.NewUserDTO;
import com.folcademy.marketapi.models.dto.UserByIdDTO;
import com.folcademy.marketapi.models.entities.UserEntity;
import com.folcademy.marketapi.models.mappers.UserMapper;
import com.folcademy.marketapi.models.repositories.UserRepository;
import org.apache.catalina.User;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class UsersService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    public UsersService(UserRepository userRepository, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }


    //view
    public ResponseEntity<?> getUserById(Long id){
        Optional<UserEntity> userEntityOptional = userRepository.findById(id);
        if (userEntityOptional.isEmpty())
            throw new NotFoundException("No existe el usuario");

        UserEntity userEntity = userEntityOptional.get();
        UserByIdDTO userByIdDTO = userMapper.mapUserEntityToUserDTO(userEntity);
        return new ResponseEntity<>(userByIdDTO, HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newUser(NewUserDTO newUserDTO){
        if (newUserDTO.getName().isEmpty())
            throw new BadRequestException("Error al crear el usuario, debe ingresar su nombre");

        if (newUserDTO.getUsername().isEmpty())
            throw new BadRequestException("Error al crear el usuario, debe ingresar el usuario");

        if (newUserDTO.getPassword().isEmpty())
            throw new BadRequestException("Error al crear el usuario, debe ingresar su contraseña");

        try{
            UserEntity userEntity = userMapper.mapUserDTOToUserEntity(newUserDTO);
            userRepository.save(userEntity);
            return new ResponseEntity<>(userEntity, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //edit
    public ResponseEntity<?> editUser(Long id, EditUserDTO user){
        try {
            if (!userRepository.existsById(id))
                throw new NotFoundException("No existe el usuario");
            UserEntity userEntity = userRepository.findById(id).get();
            if (!user.getName().isEmpty())
                userEntity.setName(user.getName());
            if (!user.getUsername().isEmpty())
                userEntity.setUsername(user.getUsername());
            if (!user.getPassword().isEmpty())
                userEntity.setPassword(user.getPassword());
            if (!user.getSubsidiaryId().toString().isEmpty())
                userEntity.setSubsidiaryId(user.getSubsidiaryId());

            userEntity.setModified(new Date());
            userRepository.save(userEntity);
            return new ResponseEntity<>("Usuario actualizado", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteUser (Long id){
        Optional<UserEntity> userEntityOptional = userRepository.findById(id);
        if (userEntityOptional.isEmpty())
            throw new NotFoundException("No existe el usuario que quiere eliminar");
        UserEntity userEntity = userEntityOptional.get();
        userEntity.setDeleted(new Date());
        userRepository.save(userEntity);
        return new ResponseEntity<>("Usuario eliminado", HttpStatus.OK);
    }
}
