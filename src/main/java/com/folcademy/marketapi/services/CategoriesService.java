package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.CategoriesByIdDTO;
import com.folcademy.marketapi.models.dto.EditCategoryDTO;
import com.folcademy.marketapi.models.dto.NewCategoryDTO;
import com.folcademy.marketapi.models.entities.CategoryEntity;
import com.folcademy.marketapi.models.mappers.CategoryMapper;
import com.folcademy.marketapi.models.repositories.CategoryRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class CategoriesService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public CategoriesService(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    //view
    public ResponseEntity<?> getCategoryById(Long id){
        if (!categoryRepository.existsById(id))
            throw new NotFoundException("No se encontro la categoria");
        CategoryEntity categoryEntity = categoryRepository.findById(id).get();
        CategoriesByIdDTO categoriesByIdDTO = categoryMapper.mapCategoryEntityToCategoryDTO(categoryEntity);
        return new ResponseEntity<>(categoriesByIdDTO,HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newCategory(NewCategoryDTO newCategoryDTO){
        if (newCategoryDTO.getName().isEmpty())
            throw new BadRequestException("Error al crear la categoria, debe ingresar el nombre de la categoria");

        try{
            CategoryEntity categoryEntity = categoryMapper.mapCategoryDTOToCategoryEntity(newCategoryDTO);
            categoryRepository.save(categoryEntity);
            return new ResponseEntity<>(categoryEntity, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editCategory(Long id, EditCategoryDTO category){
        try {
            if (!categoryRepository.existsById(id))
                return new ResponseEntity<>("No existe",HttpStatus.BAD_REQUEST);
            CategoryEntity categoryEntity = categoryRepository.findById(id).get();
            if (!category.getName().isEmpty())
                categoryEntity.setName(category.getName());

            categoryEntity.setModified(new Date());
            categoryRepository.save(categoryEntity);
            return new ResponseEntity<>("Categoria actualizado", HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteCategory (Long id){
        Optional<CategoryEntity> categoryEntityOptional = categoryRepository.findById(id);
        CategoryEntity categoryEntity = categoryEntityOptional.get();
        categoryEntity.setDeleted(new Date());
        categoryRepository.save(categoryEntity);
        return new ResponseEntity<>("Categoria eliminada", HttpStatus.OK);
    }
}
