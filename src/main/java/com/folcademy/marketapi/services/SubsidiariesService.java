package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.EditSubsidiaryDTO;
import com.folcademy.marketapi.models.dto.NewSubsidiaryDTO;
import com.folcademy.marketapi.models.dto.SubsidiaryByIdDTO;
import com.folcademy.marketapi.models.entities.SubsidiaryEntity;
import com.folcademy.marketapi.models.mappers.SubsidiaryMapper;
import com.folcademy.marketapi.models.repositories.SubsidiaryRepository;
import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class SubsidiariesService {

    private final SubsidiaryRepository subsidiaryRepository;
    private final SubsidiaryMapper subsidiaryMapper;

    public SubsidiariesService(SubsidiaryRepository subsidiaryRepository, SubsidiaryMapper subsidiaryMapper) {
        this.subsidiaryRepository = subsidiaryRepository;
        this.subsidiaryMapper = subsidiaryMapper;
    }

    //view
    public ResponseEntity<?> getSubsidiaryById(Long id){
        if (!subsidiaryRepository.existsById(id))
            throw new NotFoundException("No se encontro el subsidiario");
        Optional<SubsidiaryEntity> subsidiaryEntityOptional = subsidiaryRepository.findById(id);
        SubsidiaryEntity subsidiaryEntity = subsidiaryEntityOptional.get();
        SubsidiaryByIdDTO subsidiaryByIdDTO = subsidiaryMapper.mapSubsidiaryEntityToSubsidiaryDTO(subsidiaryEntity);
        return new ResponseEntity<>(subsidiaryByIdDTO, HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newSubsidiary(NewSubsidiaryDTO newSubsidiaryDTO){
        if (newSubsidiaryDTO.getName().isEmpty())
            throw new BadRequestException("Error al crear el subsidiario, debe ingresar el nombre");
        if (newSubsidiaryDTO.getAddress().isEmpty())
            throw new BadRequestException("Error al crear el subsidiario, debe ingresar la direccion");
        if (newSubsidiaryDTO.getCity().toString().isEmpty())
            throw new BadRequestException("Error al crear el subsidiario, debe ingresar la ciudad");
        if (newSubsidiaryDTO.getCity() == 0)
            throw new BadRequestException("Error al crear el subsidiario, el codigo de ciudad no puede ser 0");
        try {
            SubsidiaryEntity subsidiaryEntity = subsidiaryMapper.mapSubsidiaryDTOToSubsidiaryEntity(newSubsidiaryDTO);
            subsidiaryRepository.save(subsidiaryEntity);
            return new ResponseEntity<>("Subsidiario creado",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editSubsidiary(Long id, EditSubsidiaryDTO subsidiary){
        try {
            if (!subsidiaryRepository.existsById(id))
                return new ResponseEntity<>("No existe", HttpStatus.BAD_REQUEST);
            SubsidiaryEntity subsidiaryEntity = subsidiaryRepository.findById(id).get();
            if (!subsidiary.getName().isEmpty())
                subsidiaryEntity.setName(subsidiary.getName());
            if (!subsidiary.getAddress().isEmpty())
                subsidiaryEntity.setAddress(subsidiary.getAddress());
            if (!subsidiary.getCity().toString().isEmpty())
                subsidiaryEntity.setCity(subsidiary.getCity());
            subsidiaryEntity.setModified(new Date());
            subsidiaryRepository.save(subsidiaryEntity);
            return new ResponseEntity<>("Subsidiario actualizado", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteSubsidiary (Long id){
        Optional<SubsidiaryEntity> subsidiaryEntityOptional = subsidiaryRepository.findById(id);
        SubsidiaryEntity subsidiaryEntity = subsidiaryEntityOptional.get();
        subsidiaryEntity.setDeleted(new Date());
        subsidiaryRepository.save(subsidiaryEntity);
        return new ResponseEntity<>("Subsidiario eliminado", HttpStatus.OK);
    }
}
