package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.EditSaleItemDTO;
import com.folcademy.marketapi.models.dto.NewSaleItemDTO;
import com.folcademy.marketapi.models.dto.SaleItemByIdDTO;
import com.folcademy.marketapi.models.entities.SaleItemEntity;
import com.folcademy.marketapi.models.mappers.SaleItemMapper;
import com.folcademy.marketapi.models.repositories.ProductRepository;
import com.folcademy.marketapi.models.repositories.SaleItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;

@Service
public class SaleItemsService {

    @Autowired
    private SaleItemRepository saleItemRepository;
    @Autowired
    private SaleItemMapper saleItemMapper;
    @Autowired
    private ProductRepository productRepository;

    //view
    public ResponseEntity<?> getSaleItemById(Long id){
        if (!saleItemRepository.existsById(id)){
            throw new NotFoundException("No se encontro el item vendido");
        }
        SaleItemEntity saleItemEntity = saleItemRepository.findById(id).get();
        SaleItemByIdDTO saleItemByIdDTO = saleItemMapper.mapSaleItemEntityToSaelItemDTO(saleItemEntity);

        return new ResponseEntity<>(saleItemByIdDTO, HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newSaleItem (NewSaleItemDTO newSaleItemDTO){
        if (newSaleItemDTO.getAmount().toString().isEmpty())
            throw new BadRequestException("Error al crear el item vendido, debe ingresar el valor");
        if (newSaleItemDTO.getAmount().compareTo(BigDecimal.ZERO) < 0)
            throw new BadRequestException("Error al crear el item vendido, el valor no puede ser negativo");
        try {
            SaleItemEntity saleItemEntity = saleItemMapper.mapSaleItemDTOToSaleItemEntity(newSaleItemDTO);
            saleItemEntity.setProduct(productRepository.findById(newSaleItemDTO.getProductId()).get());
            saleItemRepository.save(saleItemEntity);
            return new ResponseEntity<>("Items vendidos creado",HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editSaleItem (Long id, EditSaleItemDTO saleItem){
        try {
            if (!saleItemRepository.existsById(id))
                return new ResponseEntity<>("No existe", HttpStatus.BAD_REQUEST);
            SaleItemEntity saleItemEntity =  saleItemRepository.findById(id).get();
            saleItemEntity.setAmount(saleItem.getAmount());
            saleItemRepository.save(saleItemEntity);
            return new ResponseEntity<>("Item vendido actualizado", HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteSaleItem(Long id){
        SaleItemEntity saleItemEntity = saleItemRepository.findById(id).get();
        saleItemEntity.setDeleted(new Date());
        saleItemRepository.save(saleItemEntity);
        return new ResponseEntity<>("Item vendido elminiado", HttpStatus.OK);
    }
}
