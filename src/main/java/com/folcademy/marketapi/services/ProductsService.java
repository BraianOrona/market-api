package com.folcademy.marketapi.services;

import com.folcademy.marketapi.exceptions.BadRequestException;
import com.folcademy.marketapi.exceptions.NotFoundException;
import com.folcademy.marketapi.models.dto.EditProductDTO;
import com.folcademy.marketapi.models.dto.NewProductDTO;
import com.folcademy.marketapi.models.dto.ProductsByIdDTO;
import com.folcademy.marketapi.models.entities.BrandEntity;
import com.folcademy.marketapi.models.entities.CategoryEntity;
import com.folcademy.marketapi.models.entities.ProductEntity;
import com.folcademy.marketapi.models.mappers.ProductMapper;
import com.folcademy.marketapi.models.repositories.BrandRepository;
import com.folcademy.marketapi.models.repositories.CategoryRepository;
import com.folcademy.marketapi.models.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Optional;

@Service
public class ProductsService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private BrandRepository brandRepository;


    public ProductsService(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    //view
    public ResponseEntity<?> getProductById(Long id){
        if (!productRepository.existsById(id))
            throw new NotFoundException("No se encontro el producto");
        ProductEntity productEntity = productRepository.findById(id).get();
        //ProductEntity productEntity = productEntityOptional.get();
        ProductsByIdDTO productsByIdDTO = productMapper.mapProductEntityToProductDTO(productEntity);
        return new ResponseEntity<>(productsByIdDTO,HttpStatus.OK);
    }

    //add
    public ResponseEntity<?> newProduct(NewProductDTO newProductDTO){
        if (newProductDTO.getName().isEmpty())
            throw new BadRequestException("Error al crear el producto, debe ingresar el nombre");
        if (newProductDTO.getObservations().isEmpty())
            throw new BadRequestException("Error al crear el producto, debe ingresar las observaciones");
        if (newProductDTO.getUnitAmount().toString().isEmpty())
            throw new BadRequestException("Error al crear el producto, debe ingresar el valor por unidad ");
        if (newProductDTO.getUnitAmount().compareTo(BigDecimal.ZERO) <= 0)
            throw new BadRequestException("Error al crear el producto, el valor por unidad no puede ser igual a 0 o negativo");
        if (newProductDTO.getExpirationDate().toString().isEmpty())
            throw new BadRequestException("Error al crear el producto, debe ingresar la fecha de caducidad");
        if (newProductDTO.getMeasureUnit().toString().isEmpty())
            throw new BadRequestException("Error al crear el producto, debe ingresar la unidad de medida");

        try{
            ProductEntity productEntity = productMapper.mapProductDTOToProductEntity(newProductDTO);
            Optional<CategoryEntity> categoryEntityOptional = categoryRepository.findById(newProductDTO.getCategoryId());
            if (categoryEntityOptional.isEmpty())
                throw new BadRequestException("La categoria no existe");
            productEntity.setCategory(categoryEntityOptional.get());

            Optional<BrandEntity> brandEntityOptional = brandRepository.findById(newProductDTO.getBrandId());
            if (brandEntityOptional.isEmpty())
                throw new BadRequestException("La marca no existe");
            productEntity.setBrand(brandEntityOptional.get());

            //productEntity.setBrand(brandRepository.findById(newProductDTO.getBrandId()).get());

            productRepository.save(productEntity);
            return new ResponseEntity<>(productEntity, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>("Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //edit
    public ResponseEntity<?> editProduct(Long id, EditProductDTO product){
        try {
            if (!productRepository.existsById(id))
                return new ResponseEntity<>("No existe",HttpStatus.BAD_REQUEST);
            ProductEntity productEntity = productRepository.findById(id).get();
            if (!product.getName().isEmpty())
                productEntity.setName(product.getName());
            if (!product.getObservations().isEmpty())
                productEntity.setObservations(product.getObservations());
            if (!product.getMeasureUnit().toString().isEmpty())
                productEntity.setMeasureUnit(product.getMeasureUnit());
            if (!product.getUnitAmount().toString().isEmpty())
                productEntity.setUnitAmount(product.getUnitAmount());
                productEntity.setModified(new Date());
            productRepository.save(productEntity);
            return new ResponseEntity<>("Producto actualizado", HttpStatus.OK);

        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //delete
    public ResponseEntity<String> deleteProduct (Long id){
        Optional<ProductEntity> productEntityOptional = productRepository.findById(id);
        ProductEntity productEntity = productEntityOptional.get();
        productEntity.setDeleted(new Date());
        productRepository.save(productEntity);
        return new ResponseEntity<>("Producto eliminado", HttpStatus.OK);
    }
}
