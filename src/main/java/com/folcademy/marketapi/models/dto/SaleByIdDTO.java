package com.folcademy.marketapi.models.dto;

import com.folcademy.marketapi.models.entities.SaleItemEntity;

import java.util.Date;
import java.util.List;

public class SaleByIdDTO {
    private Long id;
    private String state;
    private Long placeId;
    private Long subsidiaryId;
    private Long userId;
    private List<SaleItemEntity> saleItemId;
    private Date deleted;
    private Date created;
    private Date modified;

    public SaleByIdDTO() {
    }

    public SaleByIdDTO(Long id, String state, Long placeId, Long subsidiaryId, Long userId, List<SaleItemEntity> saleItemId, Date deleted, Date created, Date modified) {
        this.id = id;
        this.state = state;
        this.placeId = placeId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
        this.saleItemId = saleItemId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public List<SaleItemEntity> getSaleItemId() {
        return saleItemId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public void setSubsidiaryId(Long subsidiaryId) {
        this.subsidiaryId = subsidiaryId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setSaleItemId(List<SaleItemEntity> saleItemId) {
        this.saleItemId = saleItemId;
    }
}
