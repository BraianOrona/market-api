package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class UserByIdDTO {

    private Long id;
    private String name;
    private String username;
    private String password;
    private Long subsidiaryId;
    private Date created;
    private Date modified;
    private Date deleted;

    public UserByIdDTO() {
    }

    public UserByIdDTO(Long id,String name, String username, String password, Long subsidiaryId, Date created, Date modified, Date deleted) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.password = password;
        this.subsidiaryId = subsidiaryId;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSubsidiaryId(Long subsidiaryId) {
        this.subsidiaryId = subsidiaryId;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }
}
