package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class NewSaleDTO {

    private String state;
    private Long placeId;
    private Long subsidiaryId;
    private Long userId;
    private Long saleItemId;
    private Date deleted;
    private Date created;
    private Date modified;

    public NewSaleDTO() {
    }

    public NewSaleDTO(String state, Long placeId, Long subsidiaryId, Long userId, Long saleItemId, Date deleted, Date created, Date modified) {
        this.state = state;
        this.placeId = placeId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
        this.saleItemId = saleItemId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public String getState() {
        return state;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Long getSaleItemId() {
        return saleItemId;
    }
}
