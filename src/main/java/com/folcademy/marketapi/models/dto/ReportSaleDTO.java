package com.folcademy.marketapi.models.dto;

public class ReportSaleDTO {

    private String dateFrom;
    private String dateTo;

    public ReportSaleDTO() {
    }

    public ReportSaleDTO(String dateFrom, String dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }
}
