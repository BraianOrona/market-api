package com.folcademy.marketapi.models.dto;

import com.folcademy.marketapi.models.enums.ProductType;


import java.math.BigDecimal;

public class EditProductDTO {

    private String name;
    private String observations;
    private ProductType measureUnit;
    private BigDecimal unitAmount;

    public EditProductDTO() {
    }

    public EditProductDTO(String name, String observations, ProductType measureUnit, BigDecimal unitAmount) {
        this.name = name;
        this.observations = observations;
        this.measureUnit = measureUnit;
        this.unitAmount = unitAmount;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public ProductType getMeasureUnit() {
        return measureUnit;
    }

    public BigDecimal getUnitAmount() {
        return unitAmount;
    }
}
