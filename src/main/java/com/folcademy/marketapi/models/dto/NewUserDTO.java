package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class NewUserDTO {

    private String name;
    private String username;
    private String password;
    private Long subsidiaryId;
    private Date created;
    private Date modified;
    private Date deleted;

    public NewUserDTO() {
    }

    public NewUserDTO(String name, String username, String password, Long subsidiaryId, Date created, Date modified, Date deleted) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.subsidiaryId = subsidiaryId;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getDeleted() {
        return deleted;
    }
}
