package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class NewBrandDTO {

    private String name;
    private Date created;
    private Date modified;
    private Date deleted;

    public NewBrandDTO() {
    }

    public NewBrandDTO(String name, Date created, Date modified, Date deleted) {
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }
}
