package com.folcademy.marketapi.models.dto;

import com.folcademy.marketapi.models.enums.ProductType;

import java.math.BigDecimal;
import java.util.Date;

public class NewProductDTO {
    private String name;
    private String observations;
    private ProductType measureUnit;
    private BigDecimal unitAmount;
    private Long categoryId;
    private Long brandId;
    private Date expirationDate;
    private Date deleted;
    private Date created;
    private Date modified;

    public NewProductDTO() {
    }

    public NewProductDTO(String name, String observations, ProductType measureUnit, BigDecimal unitAmount, Long categoryId, Long brandId, Date expirationDate, Date deleted, Date created, Date modified) {
        this.name = name;
        this.observations = observations;
        this.measureUnit = measureUnit;
        this.unitAmount = unitAmount;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.expirationDate = expirationDate;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }


    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public ProductType getMeasureUnit() {
        return measureUnit;
    }

    public BigDecimal getUnitAmount() {
        return unitAmount;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
