package com.folcademy.marketapi.models.dto;

import com.folcademy.marketapi.models.enums.ProductType;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.math.BigDecimal;
import java.util.Date;

public class ProductsByIdDTO {

    private Long id;
    private String name;
    private String observations;
    private ProductType measureUnit;
    private BigDecimal unitAmount;
    private Long categoryId;
    private Long brandId;
    private Date expirationDate;
    private Date deleted;
    private Date created;
    private Date modified;

    public ProductsByIdDTO() {
    }

    public ProductsByIdDTO(Long id, String name, String observations, ProductType measureUnit, BigDecimal unitAmount, Long categoryId, Long brandId, Date expirationDate, Date deleted, Date created, Date modified) {
        this.id = id;
        this.name = name;
        this.observations = observations;
        this.measureUnit = measureUnit;
        this.unitAmount = unitAmount;
        this.categoryId = categoryId;
        this.brandId = brandId;
        this.expirationDate = expirationDate;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public ProductType getMeasureUnit() {
        return measureUnit;
    }

    public BigDecimal getUnitAmount() {
        return unitAmount;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public Long getBrandId() {
        return brandId;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    /*public Date getDeleted() {
        return deleted;
    }*/

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void setMeasureUnit(ProductType measureUnit) {
        this.measureUnit = measureUnit;
    }

    public void setUnitAmount(BigDecimal unitAmount) {
        this.unitAmount = unitAmount;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public void setBrandId(Long brandId) {
        this.brandId = brandId;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
