package com.folcademy.marketapi.models.dto;

import java.math.BigDecimal;

public class EditStockDTO {

    private BigDecimal amount;
    private String observations;
    private Long productId;
    private Long subsidiaryId;
    private Long userId;

    public EditStockDTO() {
    }

    public EditStockDTO(BigDecimal amount, String observations, Long productId, Long subsidiaryId, Long userId) {
        this.amount = amount;
        this.observations = observations;
        this.productId = productId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }
}
