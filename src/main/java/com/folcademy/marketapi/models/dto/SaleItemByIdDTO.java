package com.folcademy.marketapi.models.dto;

import java.math.BigDecimal;
import java.util.Date;

public class SaleItemByIdDTO {

    private Long id;
    private BigDecimal amount;
    private Long saleId;
    private Long productId;
    private Date deleted;
    private Date created;
    private Date modified;

    public SaleItemByIdDTO() {
    }

    public SaleItemByIdDTO(Long id, BigDecimal amount, Long saleId,Long productId, Date deleted, Date created, Date modified) {
        this.id = id;
        this.amount = amount;
        this.saleId = saleId;
        this.productId = productId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Long getSaleId() {
        return saleId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }
}
