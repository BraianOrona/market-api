package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class NewSubsidiaryDTO {

    private String name;
    private String address;
    private Long city;
    private Date created;
    private Date modified;
    private Date deleted;

    public NewSubsidiaryDTO() {
    }

    public NewSubsidiaryDTO(String name, String address, Long city, Date created, Date modified, Date deleted) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Long getCity() {
        return city;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getDeleted() {
        return deleted;
    }
}
