package com.folcademy.marketapi.models.dto;

import java.math.BigDecimal;
import java.util.Date;

public class NewSaleItemDTO {

    private BigDecimal amount;
    private Long saleId;
    private Long productId;
    private Date deleted;
    private Date created;
    private Date modified;

    public NewSaleItemDTO() {
    }

    public NewSaleItemDTO(BigDecimal amount, Long saleId,Long productId, Date deleted, Date created, Date modified) {
        this.amount = amount;
        this.saleId = saleId;
        this.productId = productId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Long getSaleId() {
        return saleId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Long getProductId() {
        return productId;
    }

}
