package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class SubsidiaryByIdDTO {

    private Long id;
    private String name;
    private String address;
    private Long city;
    private Date created;
    private Date modified;
    private Date deleted;

    public SubsidiaryByIdDTO() {
    }

    public SubsidiaryByIdDTO(Long id, String name, String address, Long city, Date created, Date modified, Date deleted) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Long getCity() {
        return city;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }
}
