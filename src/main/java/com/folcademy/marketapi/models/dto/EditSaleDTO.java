package com.folcademy.marketapi.models.dto;

public class EditSaleDTO {

    private String state;
    private Long placeId;
    private Long subsidiaryId;
    private Long userId;

    public EditSaleDTO() {
    }

    public EditSaleDTO(String state, Long placeId, Long subsidiaryId, Long userId) {
        this.state = state;
        this.placeId = placeId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
    }

    public String getState() {
        return state;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }
}
