package com.folcademy.marketapi.models.dto;


import com.folcademy.marketapi.models.entities.ProductEntity;

import java.util.Date;
import java.util.List;

public class BrandByIdDTO {

    private Long id;
    private String name;
    private Date created;
    private Date modified;
    private List<ProductEntity> product;

    public BrandByIdDTO() {
    }

    public BrandByIdDTO(Long id, String name, Date created, Date modified, List<ProductEntity> product) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.product = product;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public List<ProductEntity> getProduct() {
        return product;
    }

    public void setProduct(List<ProductEntity> product) {
        this.product = product;
    }
}
