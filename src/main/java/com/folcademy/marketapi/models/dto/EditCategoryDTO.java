package com.folcademy.marketapi.models.dto;

public class EditCategoryDTO {

    private String name;

    public EditCategoryDTO() {
    }

    public EditCategoryDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
