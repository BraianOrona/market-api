package com.folcademy.marketapi.models.dto;


import com.folcademy.marketapi.models.entities.ProductEntity;

import java.util.Date;
import java.util.List;

public class CategoriesByIdDTO {

    private Long id;
    private String name;
    private Date created;
    private Date modified;
    private Date deleted;
    private List<ProductEntity> product;

    public CategoriesByIdDTO() {
    }

    public CategoriesByIdDTO(Long id, String name, Date created, Date modified, Date deleted, List<ProductEntity> product) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Long getId() {
        return id;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public List<ProductEntity> getProduct() {
        return product;
    }

    public void setProduct(List<ProductEntity> product) {
        this.product = product;
    }
}
