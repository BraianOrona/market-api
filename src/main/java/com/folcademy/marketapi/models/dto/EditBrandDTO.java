package com.folcademy.marketapi.models.dto;

public class EditBrandDTO {

    private String name;

    public EditBrandDTO() {
    }

    public EditBrandDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
