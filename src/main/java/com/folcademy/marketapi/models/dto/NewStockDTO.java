package com.folcademy.marketapi.models.dto;

import java.math.BigDecimal;
import java.util.Date;

public class NewStockDTO {

    private BigDecimal amount;
    private String observations;
    private Long productId;
    private Long subsidiaryId;
    private Long userId;
    private Date deleted;
    private Date created;
    private Date modified;

    public NewStockDTO() {
    }

    public NewStockDTO(BigDecimal amount, String observations, Long productId, Long subsidiaryId, Long userId, Date deleted, Date created, Date modified) {
        this.amount = amount;
        this.observations = observations;
        this.productId = productId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
}
