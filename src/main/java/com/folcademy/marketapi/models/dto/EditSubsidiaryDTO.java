package com.folcademy.marketapi.models.dto;

public class EditSubsidiaryDTO {

    private String name;
    private String address;
    private Long city;

    public EditSubsidiaryDTO() {
    }

    public EditSubsidiaryDTO(String name, String address, Long city) {
        this.name = name;
        this.address = address;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Long getCity() {
        return city;
    }
}
