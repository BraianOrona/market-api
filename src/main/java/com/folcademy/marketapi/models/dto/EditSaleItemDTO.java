package com.folcademy.marketapi.models.dto;

import java.math.BigDecimal;

public class EditSaleItemDTO {

    private BigDecimal amount;

    public EditSaleItemDTO() {
    }

    public EditSaleItemDTO(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
