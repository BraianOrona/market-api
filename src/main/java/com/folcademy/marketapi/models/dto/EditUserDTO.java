package com.folcademy.marketapi.models.dto;

import java.util.Date;

public class EditUserDTO {

    private String name;
    private String username;
    private String password;
    private Long subsidiaryId;

    public EditUserDTO() {
    }

    public EditUserDTO(String name, String username, String password, Long subsidiaryId) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.subsidiaryId = subsidiaryId;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }
}
