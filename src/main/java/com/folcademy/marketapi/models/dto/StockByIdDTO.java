package com.folcademy.marketapi.models.dto;

import java.math.BigDecimal;
import java.util.Date;

public class StockByIdDTO {

    private Long id;
    private BigDecimal amount;
    private String observations;
    private Long productId;
    private Long subsidiaryId;
    private Long userId;
    private Date deleted;
    private Date created;
    private Date modified;

    public StockByIdDTO() {
    }

    public StockByIdDTO(Long id, BigDecimal amount, String observations, Long productId, Long subsidiaryId, Long userId, Date deleted, Date created, Date modified) {
        this.id = id;
        this.amount = amount;
        this.observations = observations;
        this.productId = productId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public Long getProductId() {
        return productId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public void setSubsidiaryId(Long subsidiaryId) {
        this.subsidiaryId = subsidiaryId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
