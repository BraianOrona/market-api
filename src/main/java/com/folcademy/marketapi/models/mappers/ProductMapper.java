package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.NewProductDTO;
import com.folcademy.marketapi.models.dto.ProductsByIdDTO;
import com.folcademy.marketapi.models.entities.ProductEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ProductMapper {
    public ProductEntity mapProductDTOToProductEntity (NewProductDTO newProductDTO){
        return new ProductEntity(
                newProductDTO.getName(),
                newProductDTO.getObservations(),
                newProductDTO.getMeasureUnit(),
                newProductDTO.getUnitAmount(),
                null,
                null,
                newProductDTO.getExpirationDate(),
                newProductDTO.getDeleted(),
                new Date(),
                newProductDTO.getModified());
    }

    public ProductsByIdDTO mapProductEntityToProductDTO (ProductEntity productEntity){
        ProductsByIdDTO productsByIdDTO = new ProductsByIdDTO();
        productsByIdDTO.setId(productEntity.getId());
        productsByIdDTO.setName(productEntity.getName());
        productsByIdDTO.setObservations(productEntity.getObservations());
        productsByIdDTO.setMeasureUnit(productEntity.getMeasureUnit());
        productsByIdDTO.setUnitAmount(productEntity.getUnitAmount());
        productsByIdDTO.setCategoryId(productEntity.getCategory().getId());
        productsByIdDTO.setBrandId(productEntity.getBrand().getId());
        productsByIdDTO.setExpirationDate(productEntity.getExpirationDate());
        productsByIdDTO.setDeleted(productEntity.getDeleted());
        productsByIdDTO.setCreated(productEntity.getCreated());
        productsByIdDTO.setModified(productEntity.getModified());
        return productsByIdDTO;
    }
}
