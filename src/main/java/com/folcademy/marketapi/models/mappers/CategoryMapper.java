package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.CategoriesByIdDTO;
import com.folcademy.marketapi.models.dto.NewCategoryDTO;
import com.folcademy.marketapi.models.entities.CategoryEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class CategoryMapper {
    public CategoryEntity mapCategoryDTOToCategoryEntity (NewCategoryDTO newCategoryDTO){
        return new CategoryEntity(newCategoryDTO.getName(),new Date(),newCategoryDTO.getModified(),newCategoryDTO.getDeleted());
    }

    public CategoriesByIdDTO mapCategoryEntityToCategoryDTO (CategoryEntity categoryEntity){
        CategoriesByIdDTO categoriesByIdDTO = new CategoriesByIdDTO();
        categoriesByIdDTO.setId(categoryEntity.getId());
        categoriesByIdDTO.setName(categoryEntity.getName());
        categoriesByIdDTO.setCreated(categoryEntity.getCreated());
        categoriesByIdDTO.setModified(categoryEntity.getModified());
        categoriesByIdDTO.setDeleted(categoryEntity.getDeleted());
        categoriesByIdDTO.setProduct(categoryEntity.getProducts());
        return categoriesByIdDTO;
    }
}
