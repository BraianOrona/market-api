package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.NewSaleItemDTO;
import com.folcademy.marketapi.models.dto.SaleItemByIdDTO;
import com.folcademy.marketapi.models.entities.SaleItemEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SaleItemMapper {
    public SaleItemEntity mapSaleItemDTOToSaleItemEntity (NewSaleItemDTO newSaleItemDTO){
        return new SaleItemEntity(newSaleItemDTO.getAmount(),
                newSaleItemDTO.getSaleId(),
                null,
                newSaleItemDTO.getDeleted(),
                new Date(),
                newSaleItemDTO.getModified());
    }

    public SaleItemByIdDTO mapSaleItemEntityToSaelItemDTO (SaleItemEntity saleItemEntity){
        SaleItemByIdDTO saleItemByIdDTO = new SaleItemByIdDTO();
        saleItemByIdDTO.setId(saleItemEntity.getId());
        saleItemByIdDTO.setAmount(saleItemEntity.getAmount());
        saleItemByIdDTO.setSaleId(saleItemEntity.getSaleId());
        saleItemByIdDTO.setProductId(saleItemEntity.getProduct().getId());
        saleItemByIdDTO.setDeleted(saleItemEntity.getDeleted());
        saleItemByIdDTO.setCreated(saleItemEntity.getCreated());
        saleItemByIdDTO.setModified(saleItemEntity.getModified());
        return saleItemByIdDTO;
    }
}
