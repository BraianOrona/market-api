package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.BrandByIdDTO;
import com.folcademy.marketapi.models.dto.NewBrandDTO;
import com.folcademy.marketapi.models.entities.BrandEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class BrandMapper {
    public BrandEntity mapBrandDTOToBrandEntity (NewBrandDTO newBrandDTO){
        return new BrandEntity(newBrandDTO.getName(),new Date(), newBrandDTO.getModified(), newBrandDTO.getDeleted());

    }

    public BrandByIdDTO mapBrandEntityToBrandDTO (BrandEntity brandEntity){
        BrandByIdDTO brandByIdDTO = new BrandByIdDTO();
        brandByIdDTO.setId(brandEntity.getId());
        brandByIdDTO.setName(brandEntity.getName());
        brandByIdDTO.setCreated(brandEntity.getCreated());
        brandByIdDTO.setModified(brandEntity.getModified());
        brandByIdDTO.setProduct(brandEntity.getProducts());
        return brandByIdDTO;

    }
}
