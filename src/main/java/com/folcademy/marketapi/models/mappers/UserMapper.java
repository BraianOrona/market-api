package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.EditUserDTO;
import com.folcademy.marketapi.models.dto.NewUserDTO;
import com.folcademy.marketapi.models.dto.UserByIdDTO;
import com.folcademy.marketapi.models.entities.UserEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserMapper {
    public UserEntity mapUserDTOToUserEntity (NewUserDTO newUserDTO){
        return new UserEntity(newUserDTO.getName(),
                newUserDTO.getUsername(),
                newUserDTO.getPassword(),
                newUserDTO.getSubsidiaryId(),
                new Date(),
                newUserDTO.getModified(),
                newUserDTO.getDeleted());
    }

    public UserByIdDTO mapUserEntityToUserDTO (UserEntity userEntity){
        UserByIdDTO userByIdDTO = new UserByIdDTO();
        userByIdDTO.setId(userEntity.getId());
        userByIdDTO.setName(userEntity.getName());
        userByIdDTO.setUsername(userEntity.getUsername());
        userByIdDTO.setPassword(userEntity.getPassword());
        userByIdDTO.setSubsidiaryId(userEntity.getSubsidiaryId());
        userByIdDTO.setCreated(userEntity.getCreated());
        userByIdDTO.setModified(userEntity.getModified());
        userByIdDTO.setDeleted(userEntity.getDeleted());
        return userByIdDTO;
    }

}
