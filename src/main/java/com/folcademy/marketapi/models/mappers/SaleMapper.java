package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.NewSaleDTO;
import com.folcademy.marketapi.models.dto.SaleByIdDTO;
import com.folcademy.marketapi.models.entities.SaleEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SaleMapper {
    public SaleEntity mapSaleDTOToSaleEntity (NewSaleDTO newSaleDTO){
        return new SaleEntity(newSaleDTO.getState(),
                newSaleDTO.getPlaceId(),
                newSaleDTO.getSubsidiaryId(),
                newSaleDTO.getUserId(),
                null,
                newSaleDTO.getDeleted(),
                new Date(),
                newSaleDTO.getModified());
    }

    public SaleByIdDTO mapSaleEntityToSaleDTO (SaleEntity saleEntity){
        SaleByIdDTO saleByIdDTO = new SaleByIdDTO();
        saleByIdDTO.setId(saleEntity.getId());
        saleByIdDTO.setState(saleEntity.getState());
        saleByIdDTO.setPlaceId(saleEntity.getPlaceId());
        saleByIdDTO.setSubsidiaryId(saleEntity.getSubsidiaryId());
        saleByIdDTO.setUserId(saleEntity.getUserId());
        saleByIdDTO.setSaleItemId(saleEntity.getSaleItems());
        saleByIdDTO.setDeleted(saleEntity.getDeleted());
        saleByIdDTO.setCreated(saleEntity.getCreated());
        saleByIdDTO.setModified(saleEntity.getModified());
        return saleByIdDTO;
    }
}
