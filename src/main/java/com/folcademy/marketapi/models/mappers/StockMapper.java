package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.NewStockDTO;
import com.folcademy.marketapi.models.dto.StockByIdDTO;
import com.folcademy.marketapi.models.entities.StockEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class StockMapper {
    public StockEntity mapStockDTOToStockEntity (NewStockDTO newStockDTO){
        return new StockEntity(newStockDTO.getAmount(),
                newStockDTO.getObservations(),
                null,
                null,
                null,
                newStockDTO.getDeleted(),
                new Date(),
                newStockDTO.getModified());
        /*StockEntity stockEntity = new StockEntity();
        stockEntity.setAmount(newStockDTO.getAmount());
        stockEntity.setObservations(newStockDTO.getObservations());
        return stockEntity;*/

    }

    public StockByIdDTO mapStockEntityToStockDTO (StockEntity stockEntity){
        StockByIdDTO stockByIdDTO = new StockByIdDTO();
        stockByIdDTO.setId(stockEntity.getId());
        stockByIdDTO.setAmount(stockEntity.getAmount());
        stockByIdDTO.setObservations(stockEntity.getObservations());
        stockByIdDTO.setProductId(stockEntity.getProductId().getId());
        stockByIdDTO.setSubsidiaryId(stockEntity.getSubsidiaryId().getId());
        stockByIdDTO.setUserId(stockEntity.getUserId().getId());
        return stockByIdDTO;
    }
}
