package com.folcademy.marketapi.models.mappers;

import com.folcademy.marketapi.models.dto.NewSubsidiaryDTO;
import com.folcademy.marketapi.models.dto.SubsidiaryByIdDTO;
import com.folcademy.marketapi.models.entities.SubsidiaryEntity;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class SubsidiaryMapper {
    public SubsidiaryEntity mapSubsidiaryDTOToSubsidiaryEntity(NewSubsidiaryDTO newSubsidiaryDTO){
        return new SubsidiaryEntity(newSubsidiaryDTO.getName(),
                newSubsidiaryDTO.getAddress(),
                newSubsidiaryDTO.getCity(),
                null,
                new Date(),
                newSubsidiaryDTO.getModified(),
                newSubsidiaryDTO.getDeleted());
    }

    public SubsidiaryByIdDTO mapSubsidiaryEntityToSubsidiaryDTO (SubsidiaryEntity subsidiaryEntity){
        SubsidiaryByIdDTO subsidiaryByIdDTO = new SubsidiaryByIdDTO();
        subsidiaryByIdDTO.setId(subsidiaryEntity.getId());
        subsidiaryByIdDTO.setName(subsidiaryEntity.getName());
        subsidiaryByIdDTO.setAddress(subsidiaryEntity.getAddress());
        subsidiaryByIdDTO.setCity(subsidiaryEntity.getCity());
        subsidiaryByIdDTO.setCreated(subsidiaryEntity.getCreated());
        subsidiaryByIdDTO.setModified(subsidiaryEntity.getModified());
        subsidiaryByIdDTO.setDeleted(subsidiaryEntity.getDeleted());
        return subsidiaryByIdDTO;
    }
}
