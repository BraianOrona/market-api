package com.folcademy.marketapi.models.enums;

public enum ProductType {
    UNIDAD, SIXPACK, CAJA
}
