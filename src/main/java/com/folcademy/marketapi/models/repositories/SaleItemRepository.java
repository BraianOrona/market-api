package com.folcademy.marketapi.models.repositories;

import com.folcademy.marketapi.models.entities.SaleItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaleItemRepository extends CrudRepository<SaleItemEntity, Long> {
}
