package com.folcademy.marketapi.models.repositories;

import com.folcademy.marketapi.models.entities.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface StockRepository extends CrudRepository<StockEntity, Long> {

    List<StockEntity> findByCreatedBetween(Date dateFrom, Date dateTo);

}
