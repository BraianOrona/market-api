package com.folcademy.marketapi.models.repositories;

import com.folcademy.marketapi.models.entities.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long> {

    //List<ProductEntity> findAllById(Long id);
}
