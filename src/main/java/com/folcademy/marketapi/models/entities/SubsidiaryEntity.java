package com.folcademy.marketapi.models.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;



@Entity(name = "subsidiaries")
public class SubsidiaryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String address;
    private Long city;
    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private List<UserEntity> users;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    private Date modified;
    private Date deleted;

    /*
    @OneToMany(mappedBy = "subsidiary",cascade = CascadeType.ALL)
    private List<UserEntity> users;

    @OneToMany(mappedBy = "subsidiary",cascade = CascadeType.ALL)
    private List<StockEntity> stocks;
    */

    public SubsidiaryEntity() {
    }

    public SubsidiaryEntity(String name, String address, Long city, List<UserEntity> users, Date created, Date modified, Date deleted) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.users = users;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public Long getCity() {
        return city;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public Date getDeleted() {
        return deleted;
    }

    public List<UserEntity> getUsers() {
        return users;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setUsers(List<UserEntity> users) {
        this.users = users;
    }
}
