package com.folcademy.marketapi.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.folcademy.marketapi.models.enums.ProductType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/*@Data
@AllArgsConstructor
@NoArgsConstructor*/

@Entity(name = "products")
public class ProductEntity {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String observations;
    @Enumerated(EnumType.STRING)
    private ProductType measureUnit;
    private BigDecimal unitAmount;
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "category_id")
    private CategoryEntity category;
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "brand_id")
    private BrandEntity brand;
    private Date expirationDate;
    private Date deleted;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    private Date modified;


    public ProductEntity() {
    }

    public ProductEntity(String name, String observations, ProductType measureUnit, BigDecimal unitAmount, CategoryEntity categoryId, BrandEntity brand, Date expirationDate, Date deleted, Date created, Date modified) {
        this.name = name;
        this.observations = observations;
        this.measureUnit = measureUnit;
        this.unitAmount = unitAmount;
        this.category = categoryId;
        this.brand = brand;
        this.expirationDate = expirationDate;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getObservations() {
        return observations;
    }

    public ProductType getMeasureUnit() {
        return measureUnit;
    }

    public BigDecimal getUnitAmount() {
        return unitAmount;
    }

    public CategoryEntity getCategory() {
        return category;
    }

    public BrandEntity getBrand() {
        return brand;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    /*
    public CategoryEntity getCategory() {
        return category;
    }

    public BrandEntity getBrand() {
        return brand;
    }

    public Set<ProductEntity> getProducts() {
        return products;
    }

    public List<SaleItemEntity> getSaleItems() {
        return saleItems;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void setMeasureUnit(ProductType measureUnit) {
        this.measureUnit = measureUnit;
    }

    public void setUnitAmount(BigDecimal unitAmount) {
        this.unitAmount = unitAmount;
    }

    public void setCategory(CategoryEntity category) {
        this.category = category;
    }

    public void setBrand(BrandEntity brand) {
        this.brand = brand;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}