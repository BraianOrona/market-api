package com.folcademy.marketapi.models.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "brands")
public class BrandEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    private Date modified;
    private Date deleted;
    @OneToMany(mappedBy = "brand",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<ProductEntity> products;

    /*
    @OneToMany(mappedBy = "brand",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<ProductEntity> products;*/

    public BrandEntity() {
    }

    public BrandEntity(String name, Date created, Date modified, Date deleted) {
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
    }

    public BrandEntity(Long id, String name, Date created, Date modified, Date deleted, List<ProductEntity> products) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
        this.products = products;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    /*public List<ProductEntity> getProducts() {
        return products;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public Date getDeleted() {
        return deleted;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public List<ProductEntity> getProducts() {
        return products;
    }

    public void setProducts(List<ProductEntity> products) {
        this.products = products;
    }
}