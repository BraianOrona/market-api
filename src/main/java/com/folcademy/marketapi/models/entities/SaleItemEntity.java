package com.folcademy.marketapi.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;



@Entity(name = "saleItems")
public class SaleItemEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private BigDecimal amount;
    private Long saleId;
    //@JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "product_id")
    private ProductEntity product;
    private Date deleted;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    private Date modified;

    /*
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "sales_id")
    private SaleEntity sales;*/


    public SaleItemEntity() {
    }

    public SaleItemEntity(BigDecimal amount, Long saleId, ProductEntity product, Date deleted, Date created, Date modified) {
        this.amount = amount;
        this.saleId = saleId;
        this.product = product;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Long getSaleId() {
        return saleId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    /*
    public SaleEntity getSales() {
        return sales;
    }

    public ProductEntity getProduct() {
        return product;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setSaleId(Long saleId) {
        this.saleId = saleId;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public ProductEntity getProduct() {
        return product;
    }

    public void setProduct(ProductEntity product) {
        this.product = product;
    }
}
