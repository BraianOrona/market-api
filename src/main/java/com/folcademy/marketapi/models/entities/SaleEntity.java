package com.folcademy.marketapi.models.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Set;



@Entity(name = "sales")
public class SaleEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private String state;
    private Long placeId;
    private Long subsidiaryId;
    private Long userId;
    @OneToMany(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "saleItem_id")
    private List<SaleItemEntity> saleItems;
    private Date deleted;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    private Date modified;

    /*
    @OneToMany(mappedBy = "sale_items",cascade = CascadeType.ALL)
    private List<SaleEntity> sales;*/


    public SaleEntity() {
    }

    public SaleEntity(String state, Long placeId, Long subsidiaryId, Long userId,List<SaleItemEntity> saleItems, Date deleted, Date created, Date modified) {
        this.state = state;
        this.placeId = placeId;
        this.subsidiaryId = subsidiaryId;
        this.userId = userId;
        this.saleItems = saleItems;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public Long getPlaceId() {
        return placeId;
    }

    public Long getSubsidiaryId() {
        return subsidiaryId;
    }

    public Long getUserId() {
        return userId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }

    public List<SaleItemEntity> getSaleItems() {
        return saleItems;
    }

    /*
    public List<SaleEntity> getSales() {
        return sales;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setPlaceId(Long placeId) {
        this.placeId = placeId;
    }

    public void setSubsidiaryId(Long subsidiaryId) {
        this.subsidiaryId = subsidiaryId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }

    public void setSaleItems(List<SaleItemEntity> saleItems) {
        this.saleItems = saleItems;
    }
}
