package com.folcademy.marketapi.models.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;



@Entity(name = "stocks")
public class StockEntity {

    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;
    private BigDecimal amount;
    private String observations;
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "product_id")
    private ProductEntity productId;
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "subsidiary_id")
    private SubsidiaryEntity subsidiary;
    @JsonIgnore
    @ManyToOne()
    @JoinColumn(name = "user_id")
    private UserEntity userId;
    private Date deleted;
    @Column(name = "date", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date created;
    private Date modified;

    /*@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    private ProductEntity products;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "subsidiary_id")
    private SubsidiaryEntity subsidiary;
*/
    public StockEntity() {
    }

    public StockEntity(BigDecimal amount, String observations, ProductEntity productId, SubsidiaryEntity subsidiaryId, UserEntity userId, Date deleted, Date created, Date modified) {
        this.amount = amount;
        this.observations = observations;
        this.productId = productId;
        this.subsidiary = subsidiaryId;
        this.userId = userId;
        this.deleted = deleted;
        this.created = created;
        this.modified = modified;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getObservations() {
        return observations;
    }

    public ProductEntity getProductId() {
        return productId;
    }

    public SubsidiaryEntity getSubsidiaryId() {
        return subsidiary;
    }

    public UserEntity getUserId() {
        return userId;
    }

    public Date getDeleted() {
        return deleted;
    }

    public Date getCreated() {
        return created;
    }

    public Date getModified() {
        return modified;
    }
/*
    public ProductEntity getProducts() {
        return products;
    }

    public SubsidiaryEntity getSubsidiary() {
        return subsidiary;
    }*/

    public void setId(Long id) {
        this.id = id;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setObservations(String observations) {
        this.observations = observations;
    }

    public void setProductId(ProductEntity productId) {
        this.productId = productId;
    }

    public void setSubsidiaryId(SubsidiaryEntity subsidiaryId) {
        this.subsidiary = subsidiaryId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    public void setDeleted(Date deleted) {
        this.deleted = deleted;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setModified(Date modified) {
        this.modified = modified;
    }
}
