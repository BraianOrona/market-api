package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.ReportSaleDTO;
import com.folcademy.marketapi.models.entities.SaleEntity;
import com.folcademy.marketapi.services.ReportsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    private ReportsService reportsService;

    @GetMapping("/sales")
    public ResponseEntity<?> getSales(@RequestParam (name = "dateFrom", defaultValue = "",required = false) String dateFrom,
                                      @RequestParam(name = "dateTo", defaultValue = "", required = false)String dateTo) throws ParseException {
        return reportsService.getSales(dateFrom,dateTo);
    }

    //@RequestParam (name = "dateFrom", defaultValue = "",required = false) String dateFrom,
    //                                                     @RequestParam(name = "dateTo", defaultValue = "", required = false)String dateTo) throws ParseException

    //@RequestBody ReportSaleDTO reportSaleDTO
    @GetMapping("")
    public ResponseEntity<?> getStocks(@RequestParam(name = "dateFrom", defaultValue = "",required = false)String dateFrom,
                                            @RequestParam(name = "dateTo", defaultValue = "", required = false)String dateTo) throws ParseException {
        return reportsService.getStocks(dateFrom,dateTo);
    }
}
