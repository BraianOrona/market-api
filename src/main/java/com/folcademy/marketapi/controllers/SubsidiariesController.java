package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.EditSubsidiaryDTO;
import com.folcademy.marketapi.models.dto.NewSubsidiaryDTO;
import com.folcademy.marketapi.models.dto.SubsidiaryByIdDTO;
import com.folcademy.marketapi.models.entities.SubsidiaryEntity;
import com.folcademy.marketapi.models.repositories.SubsidiaryRepository;
import com.folcademy.marketapi.services.SubsidiariesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subsidiaries")
public class SubsidiariesController {

    @Autowired
    private SubsidiaryRepository subsidiaryRepository;

    @Autowired
    private SubsidiariesService subsidiariesService;

    //add
    @PostMapping("/new")
    public ResponseEntity<?> newSubsidiary(@RequestBody NewSubsidiaryDTO newSubsidiaryDTO){
        return subsidiariesService.newSubsidiary(newSubsidiaryDTO);
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getSubsidiaryById(@PathVariable Long id){
        return new ResponseEntity<>(subsidiariesService.getSubsidiaryById(id), HttpStatus.OK);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editSubsidiary(@PathVariable Long id, @RequestBody EditSubsidiaryDTO editSubsidiaryDTO){
        return subsidiariesService.editSubsidiary(id,editSubsidiaryDTO);
    }

    //list
    @GetMapping("/index")
    public List<SubsidiaryEntity> index() {
        return (List<SubsidiaryEntity>) subsidiaryRepository.findAll();
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        subsidiariesService.deleteSubsidiary(id);
        return "Se elimino correctamente";
    }

}
