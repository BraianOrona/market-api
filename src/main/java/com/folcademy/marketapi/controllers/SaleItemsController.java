package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.EditSaleItemDTO;
import com.folcademy.marketapi.models.dto.NewSaleItemDTO;
import com.folcademy.marketapi.models.entities.SaleItemEntity;
import com.folcademy.marketapi.models.repositories.SaleItemRepository;
import com.folcademy.marketapi.services.SaleItemsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/saleitems")
public class SaleItemsController {

    @Autowired
    private SaleItemRepository saleItemRepository;

    @Autowired
    private SaleItemsService saleItemsService;

    //add
    @PostMapping("/new")
    public ResponseEntity<?> newSaleItem(@RequestBody NewSaleItemDTO newSaleItemDTO){
        return saleItemsService.newSaleItem(newSaleItemDTO);
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getSaleItemById(@PathVariable Long id){
        return saleItemsService.getSaleItemById(id);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editSaleItem(@PathVariable Long id,@RequestBody EditSaleItemDTO editSaleItemDTO){
        return saleItemsService.editSaleItem(id,editSaleItemDTO);
    }

    //list
    @GetMapping("/index")
    public List<SaleItemEntity> index() {
        return (List<SaleItemEntity>) saleItemRepository.findAll();
    }

    //deleted
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        saleItemsService.deleteSaleItem(id);
        return "Se elimino correctamente";
    }
}
