package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.EditStockDTO;
import com.folcademy.marketapi.models.dto.NewStockDTO;
import com.folcademy.marketapi.models.dto.StockByIdDTO;
import com.folcademy.marketapi.models.entities.StockEntity;
import com.folcademy.marketapi.models.repositories.StockRepository;
import com.folcademy.marketapi.services.StocksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/stocks")
public class StocksController {

    @Autowired
    private StockRepository stockRepository;

    @Autowired
    private StocksService stocksService;

    //add
    @PostMapping("/new")
    public ResponseEntity<?> newStock(@RequestBody NewStockDTO newStockDTO){
        return stocksService.newStock(newStockDTO);
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getStockById(@PathVariable Long id){
        return new ResponseEntity<>(stocksService.getStockById(id), HttpStatus.OK);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editStock(@PathVariable Long id, @RequestBody EditStockDTO editStockDTO){
        return stocksService.editStock(id,editStockDTO);
    }

    //list
    @GetMapping("/index")
    public List<StockEntity> index(){
        return (List<StockEntity>) stockRepository.findAll();
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        stocksService.deleteStock(id);
        return "Se elimino correctamente";
    }
}
