package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.exceptions.NotAuthorizedException;
import com.folcademy.marketapi.models.dto.*;
import com.folcademy.marketapi.models.entities.UserEntity;
import com.folcademy.marketapi.models.repositories.UserRepository;
import com.folcademy.marketapi.security.JwtUtil;
import com.folcademy.marketapi.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/users")
public class UsersController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UsersService usersService;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;

    //add
    @PostMapping("/new")
    public ResponseEntity<?> newUser(@RequestBody NewUserDTO newUserDTO){
        return usersService.newUser(newUserDTO);
    }

    //login
    @PostMapping("/login")
    public ResponseEntity<AuthenticationResponse> createToken(@RequestBody AuthenticateRequest authenticateRequest){

        UserDetails userDetails;
        try {
            userDetails = userDetailsService.loadUserByUsername(authenticateRequest.getUsername());
        }catch (UsernameNotFoundException e){
            throw new NotAuthorizedException("El usuario o contraseña es incorrecta");
        }

        if (Objects.isNull(userDetails) || !userDetails.getPassword().equals(authenticateRequest.getPassword())){{
            throw new NotAuthorizedException("El usuario o la contraseña son incorrectas");
        }}

        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(authenticateRequest.getUsername(), authenticateRequest.getPassword())
        );

        String jwt = "Bearer " + jwtUtil.generateToken(userDetails);
        AuthenticationResponse response = new AuthenticationResponse(jwt);

        return ResponseEntity.ok(response);
    }

    //logout

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable Long id){
        return new ResponseEntity<>(usersService.getUserById(id), HttpStatus.OK);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editUser(@PathVariable Long id,@RequestBody EditUserDTO editUserDTO){
        return usersService.editUser(id,editUserDTO);
    }

    //list
    @GetMapping("/index")
    public List<UserEntity> index(){
        return (List<UserEntity>) userRepository.findAll();
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        usersService.deleteUser(id);
        return "Se elimino correctamente";
    }
}
