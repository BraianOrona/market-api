package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.CategoriesByIdDTO;
import com.folcademy.marketapi.models.dto.EditCategoryDTO;
import com.folcademy.marketapi.models.dto.NewCategoryDTO;
import com.folcademy.marketapi.models.entities.CategoryEntity;
import com.folcademy.marketapi.models.repositories.CategoryRepository;
import com.folcademy.marketapi.services.CategoriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoriesController {

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CategoriesService categoriesService;

    //add
    @PostMapping("/new")
    public ResponseEntity<?> newCategory(@RequestBody NewCategoryDTO newCategoryDTO){
        return categoriesService.newCategory(newCategoryDTO);
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable Long id){
        return categoriesService.getCategoryById(id);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editCategory(@PathVariable Long id,@RequestBody EditCategoryDTO editCategoryDTO){
        return categoriesService.editCategory(id,editCategoryDTO);
    }

    //list
    @GetMapping("/index")
    public List<CategoryEntity> index(){
        return (List<CategoryEntity>) categoryRepository.findAll();
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        categoriesService.deleteCategory(id);
        return "Se elimino correctamente";
    }

}
