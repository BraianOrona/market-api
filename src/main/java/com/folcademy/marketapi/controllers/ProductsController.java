package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.EditProductDTO;
import com.folcademy.marketapi.models.dto.NewProductDTO;
import com.folcademy.marketapi.models.dto.ProductsByIdDTO;
import com.folcademy.marketapi.models.entities.ProductEntity;
import com.folcademy.marketapi.models.repositories.ProductRepository;
import com.folcademy.marketapi.services.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductsService productsService;


    //list
    @GetMapping("/index")
    public List<ProductEntity> index(){
        return (List<ProductEntity>) productRepository.findAll();
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Long id){
        return productsService.getProductById(id);
    }

    //add
    @PostMapping("/new")
    public ResponseEntity<?> newProduct(@RequestBody NewProductDTO newProductDTO){
        return productsService.newProduct(newProductDTO);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editProduct(@PathVariable Long id,@RequestBody EditProductDTO editProductDTO){
        return productsService.editProduct(id,editProductDTO);
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        productsService.deleteProduct(id);
        return "Se elimino correctamente";
    }

}
