package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.EditSaleDTO;
import com.folcademy.marketapi.models.dto.NewSaleDTO;
import com.folcademy.marketapi.models.dto.SaleByIdDTO;
import com.folcademy.marketapi.models.entities.SaleEntity;
import com.folcademy.marketapi.models.entities.SaleItemEntity;
import com.folcademy.marketapi.models.repositories.SaleRepository;
import com.folcademy.marketapi.services.SalesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    private SaleRepository saleRepository;
    @Autowired
    private SalesService salesService;

    //add
    @PostMapping("/new")
    public ResponseEntity<String> newSale(@RequestBody NewSaleDTO newSaleDTO){
        return salesService.newSale(newSaleDTO);
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getSaleById(@PathVariable Long id){
        return new ResponseEntity<>(salesService.getSaleById(id), HttpStatus.OK);
    }

    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editSale(@PathVariable Long id, @RequestBody EditSaleDTO editSaleDTO){
        return salesService.editSale(id,editSaleDTO);
    }

    //list
    @GetMapping("/index")
    public List<SaleEntity> index() {
        return (List<SaleEntity>) saleRepository.findAll();
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        salesService.deleteSale(id);
        return "Se elimino correctamente";
    }

}
