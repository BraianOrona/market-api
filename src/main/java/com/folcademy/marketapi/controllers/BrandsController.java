package com.folcademy.marketapi.controllers;

import com.folcademy.marketapi.models.dto.EditBrandDTO;
import com.folcademy.marketapi.models.dto.NewBrandDTO;
import com.folcademy.marketapi.models.entities.BrandEntity;
import com.folcademy.marketapi.models.repositories.BrandRepository;
import com.folcademy.marketapi.services.BrandsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/brands")
public class BrandsController {

    @Autowired
    private BrandRepository brandRepository;
    @Autowired
    private BrandsService brandsService;

    //add
    @PostMapping("")
    public ResponseEntity<?> newBrand(@RequestBody NewBrandDTO newBrandDTO){
        return brandsService.newBrand(newBrandDTO);
    }

    //view
    @GetMapping("/{id}")
    public ResponseEntity<?> getBrandById(@PathVariable Long id){
        return new ResponseEntity<>(brandsService.getBrandById(id), HttpStatus.OK);
    }
    //edit
    @PutMapping("/{id}")
    public ResponseEntity<?> editBrand(@PathVariable Long id,@RequestBody EditBrandDTO editBrandDTO){
        return brandsService.editBrand(id,editBrandDTO);
    }

    //list
    @GetMapping("/index")
    public List<BrandEntity> index(){
        return (List<BrandEntity>) brandRepository.findAll();
    }

    //delete
    @DeleteMapping("/{id}")
    public String delete(@PathVariable Long id){
        brandsService.deleteBrand(id);
        return "Se elimino correctamente";
    }
}
